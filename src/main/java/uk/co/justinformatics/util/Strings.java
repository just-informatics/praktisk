/*
 * Copyright (c) 2016. The Praktisk Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package uk.co.justinformatics.util;

import java.util.*;
import java.util.function.*;

/**
 * Provides:<ul>
 * <li>{@link #newOptionalString}: a method for creating an {@link Optional}
 * where an empty {@link String} is analogous to an empty Optional.</li>
 * <li>{@link #NOT_EMPTY}: a {@link Predicate} that tests whether a specified
 * {@link String} is empty.</li>
 * </ul>
 *
 * @author Gavin Foley
 */
public interface Strings {

    /**
     *  A {@link Predicate} that returns true if the specified {@link String}
     *  is empty.
     */
    Predicate<String> NOT_EMPTY = string -> !string.isEmpty();

    /**
     * Returns an {@link Optional} describing the specified {@link String},
     * if neither null or empty, otherwise returns an empty Optional.
     * @param value the possibly-null or empty string to describe
     * @return an {@link Optional} with a present value if the specified
     * {@link String} is neither null or empty, otherwise an empty Optional
     */
    static Optional<String> newOptionalString(String value) {
        return Optional.ofNullable(value).filter(NOT_EMPTY);
    }
}
