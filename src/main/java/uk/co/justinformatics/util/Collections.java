/*
 * Copyright (c) 2016. The Praktisk Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package uk.co.justinformatics.util;


import java.util.*;
import java.util.function.*;

/**
 * Provides:<ul>
 * <li>{@link #newOptionalList} and {@link #newOptionalSet}: methods for
 * creating {@link Optional}s where an empty {@link Collection} is analogous
 * to an empty Optional.</li>
 * <li>{@link #notEmpty}: a method to create A {@link Collection} based
 * {@link Predicate} that returns true when given an empty collection.</li>
 * </ul>
 *
 * @author Gavin Foley
 */
public interface Collections {

    /**
     * Returns an {@link Optional} describing the specified {@link List},
     * if neither null or empty, otherwise returns an empty Optional.
     * @param list the possibly-null or empty {@link List} to describe
     * @param <T> the type of elements in this {@link List}
     * @return an {@link Optional} with a present value if the specified
     * {@link List} is neither null or empty, otherwise an empty Optional
     */
    static <T> Optional<List<T>> newOptionalList(List<T> list) {
        return Optional.ofNullable(list).filter(notEmpty());
    }

    /**
     * Returns an {@link Optional} describing the specified {@link Set},
     * if neither null or empty, otherwise returns an empty Optional.
     * @param set the possibly-null or empty {@link Set} to describe
     * @param <T> the type of elements in this {@link Set}
     * @return an {@link Optional} with a present value if the specified
     * {@link Set} is neither null or empty, otherwise an empty Optional
     */
    static <T> Optional<Set<T>> newOptionalSet(Set<T> set) {
        return Optional.ofNullable(set).filter(notEmpty());
    }

    /**
     * Returns a {@link Predicate} that will be true when given a non empty
     * {@link Collection}, whose elements are of Type T.
     * @param <T> the type of the elements in the {@link Collection} to be
     *           passed
     * @return a {@link Predicate} that will be true when given a non empty
     * {@link Collection}
     */
    static <T> Predicate<Collection<T>> notEmpty() {
        return collection -> !collection.isEmpty();
    }
}
