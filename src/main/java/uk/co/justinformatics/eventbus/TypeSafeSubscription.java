/*
 * Copyright (c) 2016. The Praktisk Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package uk.co.justinformatics.eventbus;

import java.util.function.*;

/**
 * Implementation of {@link Subscription} that provides a type safe
 * relationship between destination and predicate.
 *
 * The predicate defines what events the supplied destination receives.
 *
 * @author Gavin Foley
 */
class TypeSafeSubscription<E extends Event> implements Subscription {

    private final Predicate<E> predicate;
    private final Destination<E> destination;
    private final Class<E> eventClass;

    TypeSafeSubscription(Predicate<E> predicate, Class<E> eventClass, Destination<E> destination) {
        this.predicate = predicate;
        this.destination = destination;
        this.eventClass = eventClass;
    }

    TypeSafeSubscription(Class<E> eventClass, Destination<E> destination) {
        this(e -> true, eventClass, destination);
    }

    @Override
    public boolean appliesTo(Event event) {
        return eventClass.isInstance(event) && predicate.test(eventClass.cast(event));
    }

    @Override
    public void fulfil(Event event) {
        destination.receive(eventClass.cast(event));
    }

    @Override
    public boolean isFor(Destination<? extends Event> candidate) {
        return destination.equals(candidate);
    }
}
