/*
 * Copyright (c) 2016. The Praktisk Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package uk.co.justinformatics.eventbus;

/**
 * When subscribed to an {@link EventBus}, Destination implementations receive events posted to that EventBus that
 * fulfil the subscription.
 *
 * @param <E> the type of Events the destination can receive
 *
 * @author Gavin Foley
 */
public interface Destination<E extends Event> {

    /**
     * Receives an event.
     *
     * @param event the Event that is received.
     */
    void receive(E event);
}
