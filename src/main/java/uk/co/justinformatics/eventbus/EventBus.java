/*
 * Copyright (c) 2016. The Praktisk Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package uk.co.justinformatics.eventbus;

import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;

/**
 * Used to post events to subscribed destinations based on a {@link Predicate}.
 *
 * @author Gavin Foley
 */
public class EventBus {

    private final Queue<Event> deadEvents = new ConcurrentLinkedQueue<>();
    private final Destination<Event> deadEventDestination = new DeadEventDestination(deadEvents);
    private final Subscriptions subscriptions = new Subscriptions(deadEventDestination);

    /**
     * Adds a destination to the bus.  If a destination is temporary then
     * removeAllSubscriptionsFor should be used to remove it, otherwise
     * the destination reference will be retained.
     * @param <E> is the type of event that subscription is for
     * @param predicate is used to match the incoming event to a destination.
     * @param eventClass the class type of the event that the
     *                   destination receives.
     * @param destination all events matching eventClass and predicate will be
     *                    delivered here.
     */
     public <E extends Event> void subscribe(Predicate<E> predicate, Class<E> eventClass, Destination<E> destination) {
        subscriptions.add(new TypeSafeSubscription<>(predicate, eventClass, destination));
    }

    /**
     * Adds a destination to the bus.  If a destination is temporary then
     * removeAllSubscriptionsFor should be used to remove it, otherwise
     * the destination reference will be retained.
     * @param <E> is the type of event that subscription is for
     * @param eventClass the class type of the event that the destination
     *                   receives.
     * @param destination all events matching eventClass will be delivered here.
     */
    public <E extends Event> void subscribe(Class<E> eventClass, Destination<E> destination) {
        subscriptions.add(new TypeSafeSubscription<>(eventClass, destination));
    }

    /**
     * Removes all subscriptions containing the destination.
     * @param <E> is the type of event that destination is for
     * @param destination subscriptions with this destination will be removed
     */
    public <E extends Event> void removeAllSubscriptionsFor(Destination<E> destination) {
        subscriptions.remove(destination);
    }

    /**
     * Delivers the event to all matching destinations.  When an event
     * is not matched to a subscription then it is placed on the
     * deadEvents queue.
     * @param <E> is the type of event that subscription is for
     * @param event an implementation of Event to post.
     */
    public <E extends Event> void post(E event) {
        subscriptions.fulfil(event);
    }

    /**
     * All unmatched events are placed on this queue.
     * @return dead event queue.
     */
    public Queue<Event> deadEvents() {
        return deadEvents;
    }
}
