/*
 * Copyright (c) 2016. The Praktisk Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package uk.co.justinformatics.eventbus;

/**
 * Defines the relationship between destination and predicate.
 *
 * @author Gavin Foley
 */
interface Subscription {
    boolean appliesTo(Event event);
    boolean isFor(Destination<? extends Event> candidate);
    void fulfil(Event event);
}
