/*
 * Copyright (c) 2016. The Praktisk Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package uk.co.justinformatics.eventbus;

import java.util.*;

/**
 * A destination for events without a destination.
 *
 * @author Gavin Foley
 */
class DeadEventDestination implements Destination<Event> {

    private final Queue<Event> deadEvents;

    DeadEventDestination(Queue<Event> deadEvents) {
        this.deadEvents = deadEvents;
    }

    @Override
    public void receive(Event event) {
        deadEvents.add(event);
    }
}
