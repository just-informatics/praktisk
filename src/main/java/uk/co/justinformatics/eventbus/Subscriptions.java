/*
 * Copyright (c) 2016. The Praktisk Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package uk.co.justinformatics.eventbus;

import java.util.*;
import java.util.function.*;

import static java.util.Collections.*;
import static java.util.stream.Collectors.*;
import static uk.co.justinformatics.util.Collections.*;

/**
 * A collection of subscriptions. Responsible for subscription addition and removal.
 * provides destinations for a given event.
 *
 * @author Gavin Foley
 */
class Subscriptions {

    private final List<Subscription> items = synchronizedList(new ArrayList<>());
    private final List<Subscription> deadSubscription;

    Subscriptions(Destination<Event> deadEventDestination) {
        deadSubscription =
                singletonList(new TypeSafeSubscription<>(Event.class, deadEventDestination));
    }

    void add(Subscription subscription) {
        items.add(subscription);
    }

    void remove(Destination<? extends Event> destination) {
        items.removeAll(matching(itemsFor(destination)));
    }

    void fulfil(Event event) {
        List<Subscription> applicable = applicableFor(event);
        applicable.forEach(s -> s.fulfil(event));
    }

    private List<Subscription> matching(Predicate<Subscription> predicate) {
        return items.stream()
                .filter(predicate)
                .collect(toList());
    }

    private List<Subscription> applicableFor(Event event) {
        Optional<List<Subscription>> targets =
                newOptionalList(matching(itemsFor(event)));
        return targets.orElse(deadSubscription);
    }

    private Predicate<Subscription> itemsFor(
            Destination<? extends Event> destination) {
        return subscription -> subscription.isFor(destination);
    }

    private Predicate<Subscription> itemsFor(Event event) {
        return subscription -> subscription.appliesTo(event);
    }
}
