/*
 * Copyright (c) 2016 The Praktisk Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.co.justinformatics.equals;

import java.util.function.*;

/**
 * Provides an equality comparison based on a {@link Predicate} to support a minimal
 * equals implementation.
 *
 * Example:
 * <pre>{@code
 *
 *  class Person {
 *    private final String name;
 *    private final Equivalent<Person> thisInstance;
 *
 *    Person(String name) {
 *      this.name = name;
 *      this.thisInstance = new Equivalent<>(this, other -> Objects.equals(name, other.name), Person.class);
 *    }
 *    ...
 *  }
 * }</pre>
 * <p>equals can then be overridden:</p>
 * <pre>{@code
 *    public boolean equals(Object o) {
 *       return thisInstance.equalTo(o);
 *    }
 * }</pre>
 *
 * @param <T> the type of the input for the Equivalent
 *
 * @author Gavin Foley
 */
public final class Equivalent<T> {

    private final T value;
    private final Predicate<T> compareTo;
    private final Class<T> clazz;

    /**
     * Creates a new instance of Equivalent for a value of type T.
     *
     * @param value the object that others will be compared too.
     * @param compareTo a {@link Predicate} that compares two objects of type T
     * @param clazz the {@link Class} of type T
     */
    public Equivalent(T value, Predicate<T> compareTo, Class<T> clazz) {
        this.value = value;
        this.compareTo = compareTo;
        this.clazz = clazz;
    }

    /**
     * Creates an Equivalent instance for a value of type T.
     *
     * <p>Equality is based on the value of hashCode().
     * Therefore an instance of type T must fulfil the contract where the
     * following is true:</p>
     * <pre>{@code
     *   this.equals(other) && this.hashCode() == other.hashCode()
     * }</pre>
     *
     * @param value the object that others will be compared too.
     * @param clazz the {@link Class} of type T
     */
    public Equivalent(T value, Class<T> clazz) {
        this(value, other -> value.hashCode() == other.hashCode(), clazz);
    }

    /**
     * <p>Indicates whether some other object is "equal to" the value.</p>
     * <p>Analogous to {@link Object#equals(Object)}</p>
     *
     * @param other the reference object which to compare.
     * @return {@code true} if the object is the same as the value
     * constructor argument
     */
    public boolean equalTo(Object other) {
        if (other == value)
             return true;
        if (other == null || other.getClass() != clazz)
            return false;

        return compareTo.test(clazz.cast(other));
    }
}
