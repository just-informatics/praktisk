/*
 * Copyright (c) 2016 The Praktisk Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.co.justinformatics.eventbus;


import org.junit.jupiter.api.*;

import java.util.function.*;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.collection.IsEmptyCollection.*;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.*;
import static org.hamcrest.core.Is.*;
import static org.hamcrest.core.IsNot.*;
import static uk.co.justinformatics.eventbus.EventReceivedMatcher.*;

/**
 * Tests for {@link EventBus}
 *
 * @author Gavin Foley
 */
@DisplayName("EventBus")
class EventBusTest {

    private EventBus bus = new EventBus();
    private MyDestination alphaDestination = new MyDestination();
    private Predicate<MyEvent> alphaPredicate = e -> e.name.equals("Alpha");
    private MyDestination allMyEvents = new MyDestination();

    @BeforeEach
    void beforeEachTest() {
        bus.subscribe(alphaPredicate, MyEvent.class, alphaDestination);
        bus.subscribe(MyEvent.class, allMyEvents);
    }

    @Test
    @DisplayName("post() delivers an Alpha MyEvent to AlphaDestination and to allMyEvents")
    void destinationsReceivesMatchingNamedEvent() {
        Event alpha = new MyEvent("Alpha");

        bus.post(alpha);

        assertThat(alphaDestination, received(alpha));
        assertThat(allMyEvents, received(alpha));
        assertThat(bus.deadEvents(), is(empty()));
    }

    @Test
    @DisplayName("post() delivers a beta MyEvent to allMyEvents and not AlphaDestination")
    void destinationReceivesMatchingEvent() {
        Event beta = new MyEvent("Beta");

        bus.post(beta);

        assertThat(allMyEvents, received(beta));
        assertThat(alphaDestination, not(received(beta)));
        assertThat(bus.deadEvents(), is(empty()));
    }

    @Test
    @DisplayName("deadEvents() receives Event without a Destination")
    void deadEventReceivesEventWithoutDestination() {
        Event deadEvent = new MyOtherEvent();

        bus.post(deadEvent);

        assertThat(bus.deadEvents(), containsInAnyOrder(deadEvent));
        assertThat(allMyEvents, not(received(deadEvent)));
        assertThat(alphaDestination, not(received(deadEvent)));
    }

    @Test
    @DisplayName("removeAllSubscriptionsFor() removes Subscription for the given Destination")
    void removeAllSubscriptionsForRemovesSubscriptionForGivenDestination() {
        bus.removeAllSubscriptionsFor(alphaDestination);
        Event alpha = new MyEvent("alpha");

        bus.post(alpha);

        assertThat(allMyEvents, received(alpha));
        assertThat(alphaDestination, not(received(alpha)));
        assertThat(bus.deadEvents(), is(empty()));
    }
}
