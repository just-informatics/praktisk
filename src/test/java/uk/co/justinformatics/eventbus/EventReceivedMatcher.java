package uk.co.justinformatics.eventbus;


import org.hamcrest.*;

/**
 * Custom hamcrest matcher used to test {@link EventBus}
 *
 * @author Gavin Foley
 */
class EventReceivedMatcher extends TypeSafeMatcher<MyDestination> {

    final Event event;

    private EventReceivedMatcher(Event event) {
        this.event = event;
    }

    static EventReceivedMatcher received(Event event) {
        return new EventReceivedMatcher(event);
    }

    @Override
    public void describeTo(Description description) {
        description.appendText(" received event ").appendText(event.toString());
    }

    @Override
    protected boolean matchesSafely(MyDestination item) {
        return item.eventReceived()
                .filter(event::equals)
                .isPresent();
    }
}
