/*
 * Copyright (c) 2016. The Praktisk Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package uk.co.justinformatics.equals;

import java.util.Objects;

/**
 * Used by {@link EquivalentTest} to test {@link Equivalent}
 *
 * @author Gavin Foley
 */
class Person {

    private final String name;
    private final Equivalent<Person> thisInstance;

    Person(String name) {
        this.name = name;
        this.thisInstance = new Equivalent<>(this, other -> Objects.equals(name, other.name), Person.class);
    }

    @Override
    public boolean equals(Object o) {
        return thisInstance.equalTo(o);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(name);
    }
}
