/*
 * Copyright (c) 2016. The Praktisk Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package uk.co.justinformatics.equals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


/**
 * Tests for {@link Equivalent}
 *
 * @author Gavin Foley
 */
@DisplayName("Equivalent")
class EquivalentTest {

    @Nested
    @DisplayName("with Equivalent implemented in Person")
    class WithPerson {

        @Test
        @DisplayName("equalTo returns false when given null")
        void equalityIsFalseWhenGivenNull() {
            Person person = new Person("Joesph");

            assertFalse(person.equals(null));
        }

        @Test
        @DisplayName("equalTo returns false when given an object ofNullable a different class")
        void equalityIsFalseWhenGivenObjectOfDifferentClass() {
            Person person = new Person("Joesph");

            assertFalse(person.equals("joesph"));
        }

        @Test
        @DisplayName("equalTo returns false when given a different object ofNullable the same class")
        void equalityIsFalseWhenGivenDifferentObject() {
            Person person = new Person("Joesph");
            Person other = new Person("Martin");

            assertFalse(person.equals(other));
        }

        @Test
        @DisplayName("equalTo returns true when given a different instance ofNullable the same object")
        void equalityIsTrueWhenGivenDifferentInstanceOfSameObject() {
            Person person = new Person("Joseph");
            Person other = new Person("Joseph");

            assertTrue(person.equals(other));
        }

        @Test
        @DisplayName("equalTo returns true when given the same object")
        void equalityIsTrueWhenGivenSameObject() {
            Person person = new Person("Joseph");

            assertTrue(person.equals(person));
        }
    }

    @Test
    @DisplayName("constructor creates equivalent with hashCode comparison")
    void equalsToComparesWithHashCode() {
        Equivalent<String> equivalent = new Equivalent<>("Thomas", String.class);

        assertTrue(equivalent.equalTo("Thomas"));
        assertFalse(equivalent.equalTo("Martin"));
    }
}

