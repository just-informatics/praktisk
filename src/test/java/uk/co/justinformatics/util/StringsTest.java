/*
 * Copyright (c) 2016. The Praktisk Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package uk.co.justinformatics.util;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static uk.co.justinformatics.util.Assert.assertEmpty;
import static uk.co.justinformatics.util.Strings.newOptionalString;

/**
 * tests for {@link Strings}
 *
 * @author Gavin Foley
 */
@DisplayName("Strings")
class StringsTest {

    @DisplayName("newOptionalString")
    @Nested
    class NewOptionalString {

        @Test
        @DisplayName("returns a present optional for a non empty string")
        void newOptionalStringReturnsPresentOptionalForNonEmptyString() {
            final String present = "present";
            Optional<String> optional = newOptionalString(present);

            assertEquals(present, optional.get());
        }

        @Test
        @DisplayName("returns optional empty for empty String and null")
        void newOptionalStringReturnsOptionalEmptyForEmptyStringAndNull() {
            assertEmpty(newOptionalString(null),  newOptionalString(""));
        }
    }
}
