/*
 * Copyright (c) 2016. The Praktisk Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package uk.co.justinformatics.util;

import java.util.Optional;
import java.util.function.Predicate;

import static java.util.Arrays.stream;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Specialised testing assertions
 *
 * @author Gavin Foley
 */
public interface Assert {

    static <T> void assertEmpty(Optional<T>... emptyOptionals) {
        Predicate<Optional<T>> optionalEmpty = optional -> optional.equals(Optional.empty());
        assertTrue(stream(emptyOptionals).allMatch(optionalEmpty));
    }
}
