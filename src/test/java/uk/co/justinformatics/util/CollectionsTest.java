/*
 * Copyright (c) 2016. The Praktisk Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package uk.co.justinformatics.util;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static java.util.Arrays.asList;
import static java.util.Collections.emptySet;
import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static uk.co.justinformatics.util.Assert.assertEmpty;
import static uk.co.justinformatics.util.Collections.newOptionalList;
import static uk.co.justinformatics.util.Collections.newOptionalSet;

/**
 * tests for {@link Collections}
 *
 * @author Gavin Foley
 */
@DisplayName("Collections")
class CollectionsTest {

    static final List<String> NAMES = asList("Thomas", "Martin", "Maureen");

    @Nested
    @DisplayName("newOptionalList")
    class NewOptionalList {

        @Test
        @DisplayName("returns a present optional for a non empty list")
        void newOptionalListReturnsPresentOptionalForNonEmptyList() {
            Optional<List<String>> optional = newOptionalList(NAMES);

            assertEquals(NAMES, optional.get());
        }

        @Test
        @DisplayName("returns optional empty for empty lists and null")
        void newOptionalListReturnsOptionalEmptyForEmptyListAndNull() {
            assertEmpty(newOptionalList(null), newOptionalList(emptyList()));
        }
    }

    @Nested
    @DisplayName("newOptionalSet")
    class NewOptionalSet {

        @Test
        @DisplayName("returns a present optional for a non empty set")
        void newOptionalSetReturnsPresentOptionalForNonEmptyList() {
            Set<String> set = new HashSet<>(NAMES);
            Optional<Set<String>> optional = newOptionalSet(set);

            assertEquals(set, optional.get());
        }

        @Test
        @DisplayName("returns optional empty for empty sets and null")
        void newOptionalSetReturnsOptionalEmptyForEmptyListAndNull() {
            assertEmpty(newOptionalSet(null), newOptionalSet(emptySet()));
        }
    }
}
