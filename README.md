# Praktisk: A Useful Java Library

*Praktisk - a Swedish and Danish adjective that means: practical, handy, useful, working, utilitarian, utility.*

The Praktisk project is a small useful Java library that aims to:

* provide an example of Java written in a minimal style, that's easy to change,
* serve a demonstration on how to use JUnit 5.

Requires JDK 1.8 or higher.

### Latest release
--------------
The most recent release is Praktisk 1.1.1, released January 15, 2017.

To add a dependency using Gradle:

```
dependencies {
  compile 'uk.co.justinformatics:praktisk:1.1.1'
}
```

To add a dependency on Praktisk using Maven, use the following:

```xml
<dependency>
  <groupId>uk.co.justinformatics</groupId>
  <artifactId>praktisk</artifactId>
  <version>1.1.1</version>
</dependency>
```

### Learn about Praktisk
------------------
* Our [user guide](https://gitlab.com/just-informatics/praktisk/wikis/home)
* Blog posts mentioning Praktisk
    * [Minimising equals Implementations In Java](https://www.justinformatics.co.uk/blog/posts/minimising-equals-implementations-in-java) by [Gavin Foley](https://www.justinformatics.co.uk/blog/authors/gavin-foley)
    * [Describing Empty Collections And Strings As Optional.empty In Java With Praktisk](https://www.justinformatics.co.uk/blog/posts/describing-empty-collections-and-strings-as-optional.empty-in-java-with-praktisk) by [Gavin Foley](https://www.justinformatics.co.uk/blog/authors/gavin-foley)
    * [Grouping Unit Tests in Java With JUnit 5’s @Nested Annotation](https://www.justinformatics.co.uk/blog/posts/grouping-unit-tests-in-java-with-junit-5’s-@nested-annotation) by [Gavin Foley](https://www.justinformatics.co.uk/blog/authors/gavin-foley)
    * [Two Of The Things That I Like In JUnit 5](https://www.justinformatics.co.uk/blog/posts/two-of-the-things-that-i-like-in-junit-5) by [Gavin Foley](https://www.justinformatics.co.uk/blog/authors/gavin-foley)
